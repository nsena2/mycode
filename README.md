# mycode (Project Title)

Learning about using GitLab to track my ansible automation code. This code will also be used for learning git version controlling.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine
for development and testing purposes. See deployment for notes on how to deploy the project
on a live system.

### Prerequisites

* Ansible
* Python

## Built With

* [Ansible](https://www.ansible.com) - The coding language used
* [Python](https://www.python.org/) - The coding language used

## Authors

* **Noah Sena** - *Initial work* 
